PCBNEW-LibModule-V1  pią, 5 gru 2014, 14:11:09
# encoding utf-8
Units mm
$INDEX
BLADE_FUSE_E
C2V12,6
C3V18
SS008B02P22
SWDIP2
TO220GDS_VERT
TO220SDG
crystal_4pad_10x4mm
$EndINDEX
$MODULE BLADE_FUSE_E
Po 0 0 0 15 54744FCB 00000000 ~~
Li BLADE_FUSE_E
Kw ISO 8820-3 Type E
Sc 0
AR 
Op 0 0 0
T0 -6 6 1 1 0 0.15 N V 21 N "BLADE_FUSE_E"
T1 2 6 1 1 0 0.15 N V 21 N "VAL**"
DS -14.6 4.45 -14.6 3.35 0.15 21
DS -14.6 -4.45 -14.6 -3.35 0.15 21
DS 14.6 4.45 14.6 3.35 0.15 21
DS 14.6 -4.45 14.6 -3.35 0.15 21
DS 14.6 4.45 -14.6 4.45 0.15 21
DS -14.6 -4.45 14.6 -4.45 0.15 21
$PAD
Sh "1" R 14 6 0 0 0
Dr 8.7 0 0 O 8.7 1.1
At STD N 00E0FFFF
Ne 0 ""
Po -10.55 0
$EndPAD
$PAD
Sh "2" O 14 6 0 0 0
Dr 8.7 0 0 O 8.7 1.1
At STD N 00E0FFFF
Ne 0 ""
Po 10.55 0
$EndPAD
$EndMODULE BLADE_FUSE_E
$MODULE C2V12,6
Po 0 0 0 15 5481AECA 00000000 ~~
Li C2V12,6
Cd Condensateur polarise
Kw CP
Sc 0
AR 
Op 0 0 0
T0 0 2.54 1.27 1.27 0 0.254 N V 21 N "C2V10"
T1 0 -2.54 1.27 1.27 0 0.254 N V 21 N "C***"
DC 0 0 6.3 0 0.3048 21
$PAD
Sh "1" R 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.54 0
$EndPAD
$PAD
Sh "2" C 1.778 1.778 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.54 0
$EndPAD
$SHAPE3D
Na "discret/c_vert_c2v10.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE C2V12,6
$MODULE C3V18
Po 0 0 0 15 5472FD7E 00000000 ~~
Li C3V18
Cd Condensateur e = 1 pas
Kw C
Sc 0
AR 
Op 0 0 0
T0 0 -2.794 0.762 0.762 0 0.127 N V 21 N "C3V18"
T1 0 2.286 0.762 0.635 0 0.127 N V 21 N "C***"
DC 0 0 9.398 0 0.15 21
T2 -8.128 0 0.762 0.762 0 0.2032 N V 21 N "+"
$PAD
Sh "1" R 3 3 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3.81 0
$EndPAD
$PAD
Sh "2" C 3 3 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3.81 0
$EndPAD
$SHAPE3D
Na "discret/c_vert_c1v5.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE C3V18
$MODULE SS008B02P22
Po 0 0 0 15 53735ACB 00000000 ~~
Li SS008B02P22
Kw SWITCH DPDT
Sc 0
AR 
Op 0 0 0
T0 0 -4.20116 1.016 1.016 0 0.254 N V 21 N "SS008B02P22"
T1 0 4.39928 1.016 1.016 0 0.1905 N I 21 N "VAL**"
DS 4.09956 -3.40106 4.09956 -1.80086 0.2032 21
DS 4.09956 3.40106 4.09956 1.80086 0.2032 21
DS -4.09956 3.40106 -4.09956 1.80086 0.2032 21
DS -4.09956 -3.40106 -4.09956 -1.80086 0.2032 21
DS -4.09956 -3.40106 4.09956 -3.40106 0.2032 21
DS 4.09956 3.40106 -4.09956 3.40106 0.2032 21
$PAD
Sh "1" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.99898 1.24968
$EndPAD
$PAD
Sh "2" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 1.24968
$EndPAD
$PAD
Sh "3" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.99898 1.24968
$EndPAD
$PAD
Sh "4" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.99898 -1.24968
$EndPAD
$PAD
Sh "5" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -1.24968
$EndPAD
$PAD
Sh "6" O 1.6002 1.99898 0 0 0
Dr 1.00076 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.99898 -1.24968
$EndPAD
$PAD
Sh "0" O 1.99898 2.79908 0 0 0
Dr 1.39954 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4.09956 0
$EndPAD
$PAD
Sh "0" O 1.99898 2.79908 0 0 0
Dr 1.39954 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4.09956 0
$EndPAD
$EndMODULE SS008B02P22
$MODULE SWDIP2
Po 0 0 0 15 538B12E2 00000000 ~~
Li SWDIP2
Cd Switch Dil 4 elements
Kw SWITCH DEV
Sc 0
AR 
Op 0 0 0
T0 0 6.35 1.27 1.524 0 0.3048 N V 21 N "SWDIP2"
T1 0 -6.35 1.27 1.524 0 0.3048 N V 21 N "SW**"
DS -3.81 5.08 3.81 5.08 0.381 21
DS -2.54 1.27 2.54 1.27 0.381 21
DS -2.54 -1.27 2.54 -1.27 0.381 21
DS -3.81 -5.08 3.81 -5.08 0.381 21
DS 2.54 -1.27 2.54 1.27 0.381 21
DS -2.54 1.27 -2.54 -1.27 0.381 21
DS -3.81 5.08 -3.81 -5.08 0.381 21
DS 3.81 -5.08 3.81 5.08 0.381 21
$PAD
Sh "1" R 1.524 1.524 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 3.81
$EndPAD
$PAD
Sh "2" C 1.524 1.524 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 3.81
$EndPAD
$PAD
Sh "3" C 1.524 1.524 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1.27 -3.81
$EndPAD
$PAD
Sh "4" C 1.524 1.524 0 0 0
Dr 0.8128 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1.27 -3.81
$EndPAD
$EndMODULE SWDIP2
$MODULE TO220GDS_VERT
Po 0 0 0 15 54759700 00000000 ~~
Li TO220GDS_VERT
Cd Regulateur TO220 serie LM78xx
Kw TR TO220
Sc 0
AR 
Op 0 0 0
T0 -3.175 0 1.524 1.016 900 0.2032 N V 21 N "TO220_VERT"
T1 0.635 -6.35 1.524 1.016 0 0.2032 N V 21 N "VAL**"
DS 1.905 -5.08 2.54 -5.08 0.381 21
DS 2.54 -5.08 2.54 5.08 0.381 21
DS 2.54 5.08 1.905 5.08 0.381 21
DS -1.905 -5.08 1.905 -5.08 0.381 21
DS 1.905 -5.08 1.905 5.08 0.381 21
DS 1.905 5.08 -1.905 5.08 0.381 21
DS -1.905 5.08 -1.905 -5.08 0.381 21
$PAD
Sh "G" O 2.54 2.032 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2.54
$EndPAD
$PAD
Sh "D" O 2.54 2.032 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "S" R 2.54 2.032 0 0 0
Dr 1.016 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 2.54
$EndPAD
$EndMODULE TO220GDS_VERT
$MODULE TO220SDG
Po 0 0 0 15 547450F2 00000000 ~~
Li TO220SDG
Cd Transistor VMOS Irf530, TO220
Kw TR TO220 DEV
Sc 0
AR 
Op 0 0 0
T0 6.985 0 1.524 1.016 900 0.2032 N V 21 N "TO220SDG"
T1 10.795 0 1.524 1.016 900 0.2032 N V 21 N "VAL*"
DS 12.7 2.54 12.7 3.81 0.3048 21
DS 12.7 0 12.7 1.27 0.3048 21
DS 12.7 -2.54 12.7 -1.27 0.3048 21
DS 12.7 -5.08 12.7 -3.81 0.3048 21
DS 5.08 -2.54 5.08 -3.81 0.3048 21
DS 5.08 0 5.08 -1.27 0.3048 21
DS 5.08 2.54 5.08 1.27 0.3048 21
DS 5.08 5.08 5.08 3.81 0.3048 21
DS 7.62 5.08 6.35 5.08 0.3048 21
DS 10.16 5.08 8.89 5.08 0.3048 21
DS 12.7 5.08 11.43 5.08 0.3048 21
DS 15.24 5.08 13.97 5.08 0.3048 21
DS 17.78 5.08 16.51 5.08 0.3048 21
DS 20.32 5.08 19.05 5.08 0.3048 21
DS 20.32 2.54 20.32 3.81 0.3048 21
DS 20.32 0 20.32 1.27 0.3048 21
DS 20.32 -2.54 20.32 -1.27 0.3048 21
DS 20.32 -5.08 20.32 -3.81 0.3048 21
DS 17.78 -5.08 19.05 -5.08 0.3048 21
DS 15.24 -5.08 16.51 -5.08 0.3048 21
DS 12.7 -5.08 13.97 -5.08 0.3048 21
DS 10.16 -5.08 11.43 -5.08 0.3048 21
DS 7.62 -5.08 8.89 -5.08 0.3048 21
DS 5.08 -5.08 6.35 -5.08 0.3048 21
DS 0 -2.54 5.08 -2.54 0.3048 21
DS 0 0 5.08 0 0.3048 21
DS 0 2.54 5.08 2.54 0.3048 21
$PAD
Sh "G" O 2.54 2.032 0 0 0
Dr 1.143 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 2.54
$EndPAD
$PAD
Sh "D" O 2.54 2.032 0 0 0
Dr 1.143 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "S" R 2.54 2.032 0 0 0
Dr 1.143 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2.54
$EndPAD
$PAD
Sh "" C 5 5 0 0 0
Dr 5 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 16.51 0
$EndPAD
$SHAPE3D
Na "discret/to220_horiz.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE TO220SDG
$MODULE crystal_4pad_10x4mm
Po 0 0 0 15 5321E133 00000000 ~~
Li crystal_4pad_10x4mm
Sc 0
AR crystal_4pad_10x4mm
Op 0 0 0
T0 0 -3.683 1.524 1.524 0 0.3048 N V 21 N "crystal_4pad_10x4mm"
T1 0.127 3.556 1.524 1.524 0 0.3048 N V 21 N "VAL**"
DS 4.826 -0.762 4.826 0.635 0.20066 21
DS -4.699 -0.762 -4.699 0.635 0.20066 21
DS -4.826 -2.032 4.826 -2.032 0.20066 21
DS -4.826 2.032 4.826 2.032 0.20066 21
$PAD
Sh "1" R 3.048 0.5842 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.937 1.397
$EndPAD
$PAD
Sh "2" R 3.048 0.5842 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.937 1.397
$EndPAD
$PAD
Sh "2" R 3.048 0.5842 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.937 -1.397
$EndPAD
$PAD
Sh "1" R 3.048 0.5842 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.937 -1.397
$EndPAD
$EndMODULE crystal_4pad_10x4mm
$EndLIBRARY

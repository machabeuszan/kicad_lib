PCBNEW-LibModule-V1  czw, 11 gru 2014, 11:42:14
# encoding utf-8
Units mm
$INDEX
WAGO_254-xxx
$EndINDEX
$MODULE WAGO_254-xxx
Po 0 0 0 15 54897504 00000000 ~~
Li WAGO_254-xxx
Sc 0
AR 
Op 0 0 0
T0 0 5.2 1 1 0 0.15 N V 21 N "WAGO_254-xxx"
T1 -2.1 -0.5 1 1 900 0.15 N V 21 N "VAL**"
DS 1.7 -6.3 1.7 -5.3 0.15 21
DS 1.7 -5.3 -2.7 -5.3 0.15 21
DS -2.7 -5.3 -2.7 -6.3 0.15 21
DS -2.7 -6.3 1.7 -6.3 0.15 21
DS 1.7 -7.1 1.7 -6.6 0.15 21
DS -2.7 -7.1 -2.7 -6.6 0.15 21
DS -3 -6.6 2 -6.6 0.15 21
DA -0.5 -7.1 -2.7 -7.1 1800 0.15 21
DS 2 -5 2 -9.6 0.15 21
DS 2 -9.6 -3 -9.6 0.15 21
DS -3 -9.6 -3 -5 0.15 21
DS 2 3.9 2 -5 0.15 21
DS 2 -5 -3 -5 0.15 21
DS -3 -5 -3 3.9 0.15 21
DS 2 3.9 -3 3.9 0.15 21
$PAD
Sh "1" C 2.54 2.54 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 2.5
$EndPAD
$PAD
Sh "1" R 2.54 2.54 0 0 0
Dr 1.2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -0.1 -2.5
$EndPAD
$EndMODULE WAGO_254-xxx
$EndLIBRARY

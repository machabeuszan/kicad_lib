PCBNEW-LibModule-V1  sob, 8 mar 2014, 22:24:10
# encoding utf-8
$INDEX
MSOP10
MSOP8
$EndINDEX
$MODULE MSOP8
Po 0 0 0 15 531B8470 00000000 ~~
Li MSOP8
Cd 8-Lead Plastic Micro Small Outline Package (MSOP)
Sc 00000000
AR
Op 0 0 0
T0 -600 -354 350 350 2700 88 N V 21 N "U"
T1 1368 -738 350 350 900 88 N V 21 N "MSOP8"
DS -207 -1398 974 -1398 39 21
DS 974 -1398 974 -217 39 21
DS -207 -217 -207 -1398 39 21
DS -206 -217 975 -217 39 21
$PAD
Sh "1" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 41
$EndPAD
$PAD
Sh "2" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 256 39
$EndPAD
$PAD
Sh "3" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 512 39
$EndPAD
$PAD
Sh "4" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 768 39
$EndPAD
$PAD
Sh "5" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 768 -1652
$EndPAD
$PAD
Sh "6" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 512 -1652
$EndPAD
$PAD
Sh "7" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 256 -1652
$EndPAD
$PAD
Sh "8" R 160 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -1654
$EndPAD
$EndMODULE  MSOP8
$MODULE MSOP10
Po 0 0 0 15 531B8A76 00000000 ~~
Li MSOP10
Cd 10-Lead Plastic Micro Small Outline Package (MSOP)
Sc 00000000
AR MSOP10
Op 0 0 0
.LocalClearance 80
T0 -600 -354 350 350 2700 88 N V 21 N "U"
T1 1368 -738 350 350 900 88 N V 21 N "MSOP10"
DS -207 -1398 974 -1398 39 21
DS 974 -1398 974 -217 39 21
DS -207 -217 -207 -1398 39 21
DS -206 -217 975 -217 39 21
$PAD
Sh "1" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 41
$EndPAD
$PAD
Sh "2" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 197 39
$EndPAD
$PAD
Sh "3" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 394 39
$EndPAD
$PAD
Sh "4" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 591 39
$EndPAD
$PAD
Sh "7" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 591 -1652
$EndPAD
$PAD
Sh "8" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 394 -1652
$EndPAD
$PAD
Sh "9" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 197 -1652
$EndPAD
$PAD
Sh "10" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -1654
$EndPAD
$PAD
Sh "6" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 787 -1652
$EndPAD
$PAD
Sh "5" R 120 400 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 787 39
$EndPAD
$EndMODULE  MSOP10
$EndLIBRARY
